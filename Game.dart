import 'dart:io';
import 'dart:math';

class Game {
  var s = 0;
  var k = 0;
  var p = 1;
  var t = 1;
  var mode;
  var num;
  var end = 0;
  var arr3_3 = [0,0,0,0,0,0,0,0,0];
  var arr5_5 = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
  var arr3_3_s = [0,0,0,0,0,0,0,0,0];
  var arr5_5_s = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
  setRowCol(int row, int col) {
    var rows = row;
    var cols = col;
    var list = new List<int>.generate(cols * rows, (int index) => index);
    list.shuffle();

    final arr = List.generate(rows,
        (i) => List.generate(cols, (j) => list[i + j * cols] + 1, growable: false),
        growable: false);
    
    for(int i = 0; i < arr.length; i++) {
      for(int j = 0; j < arr.length; j++) {
        if (s == 1) {
          arr3_3[k] = arr[i][j];
        } else if (s == 2) {
          arr5_5[k] = arr[i][j];
        }
        k = k + 1;
      }
    }
  }
  void inputRowCol () {
    int? n1 = int.parse(stdin.readLineSync()!);
    if (n1 == 1) {
      s = 1;
      setarr3_3();
    } else if (n1 == 2) {
      s = 2;
      setarr5_5();
    }
  }
  void setarr3_3 () {
    setRowCol(3, 3);
    mode = 3;
  }
  void setarr5_5 () {
    setRowCol(5, 5);
    mode = 5;
  }
  RandomOrder (int mode) {
    Random r = new Random();
    int low = 1;
    int high = mode * mode;
    num = r.nextInt(high - low) + low;
  }
  void showRandomOrder () {
    print(num);
  }
  checkWin (int no) {
    if (no == num) {
      isWin();
    }
  }
  isWin () {
    end = 1;
  }
  void win () {
    print('ผู้ชนะ : P$p');
  }
  void loss () {
    switchPlayer();
    print('ผู้แพ้ : P$p');
  }
  getTable() {
    for(int i = 0; i < mode; i++) {
      for(int j = 0; j < mode; j++) {
        stdout.write('0 ');
      }
      print('');
    }
  }
  getCurrentPlayer () {
    return 1;
  }
  void switchPlayer () {
    if (p == 1) {
      p = 2;
    } else if (p == 2) {
      p = 1;
    }
    return;
  }
  void showTurn () {
    t = t + 1;
    return;
  }
  void showwin() {
    win();
  }
  isFinish (int n) {
    if (n == 1){
        List<String> item = ['คำสั่งสุ่มที่ได้ : เต้น1ท่า', 'คำสั่งสุ่มที่ได้ :ร้องเพลง 1 เพลง', 'คำสั่งสุ่มที่ได้ :ผู้เล่นดื่ม 2 ฝา', 'คำสั่งสุ่มที่ได้ :ผู้เล่นดื่ม 3 ฝา'];
        final _random = new Random();
        var element = item[_random.nextInt(item.length)];
        print(element);
    }else if (n == 2){
        List<String> item = ['คำสั่งสุ่มที่ได้ : ทำท่าสะพานโค้ง', 'คำสั่งสุ่มที่ได้ :ผู้เล่นดื่มวิดพื้น 1 ครั้ง', 'คำสั่งสุ่มที่ได้ :ผู้เล่นดื่ม 4 ฝา', 'คำสั่งสุ่มที่ได้ :ผู้เล่นดื่ม 3 ฝา'];
        final _random = new Random();
        var element = item[_random.nextInt(item.length)];
        print(element);
    }
     
    
  }
  void showTable (String? arr) {
    if (mode == 3) {
      if (arr == '1 1') {
        arr3_3_s[0] = 1;
      } else if (arr == '1 2') {
        arr3_3_s[1] = 1;
      } else if (arr == '1 3') {
        arr3_3_s[2] = 1;
      } else if (arr == '2 1') {
        arr3_3_s[3] = 1;
      } else if (arr == '2 2') {
        arr3_3_s[4] = 1;
      } else if (arr == '2 3') {
        arr3_3_s[5] = 1;
      } else if (arr == '3 1') {
        arr3_3_s[6] = 1;
      } else if (arr == '3 2') {
        arr3_3_s[7] = 1;
      } else if (arr == '3 3') {
        arr3_3_s[8] = 1;
      }
      for (var i = 0; i < 3; i++) {
        if (arr3_3_s[i] == 1) {
          stdout.write('${arr3_3[i]} ');
          checkWin(arr3_3[i]);
        } else {
          stdout.write('0 ');
        }
      }
      print('');
      for (var i = 3; i < 6; i++) {
        if (arr3_3_s[i] == 1) {
          stdout.write('${arr3_3[i]} ');
          checkWin(arr3_3[i]);
        } else {
          stdout.write('0 ');
        }
      }
      print('');
      for (var i = 6; i < 9; i++) {
        if (arr3_3_s[i] == 1) {
          stdout.write('${arr3_3[i]} ');
          checkWin(arr3_3[i]);
        } else {
          stdout.write('0 ');
        }
      }
      print('');
    } else if (mode == 5) {
      if (arr == '1 1') {
        arr5_5_s[0] = 1;
      } else if (arr == '1 2') {
        arr5_5_s[1] = 1;
      } else if (arr == '1 3') {
        arr5_5_s[2] = 1;
      } else if (arr == '1 4') {
        arr5_5_s[3] = 1;
      } else if (arr == '1 5') {
        arr5_5_s[4] = 1;
      } else if (arr == '2 1') {
        arr5_5_s[5] = 1;
      } else if (arr == '2 2') {
        arr5_5_s[6] = 1;
      } else if (arr == '2 3') {
        arr5_5_s[7] = 1;
      } else if (arr == '2 4') {
        arr5_5_s[8] = 1;
      } else if (arr == '2 5') {
        arr5_5_s[9] = 1;
      } else if (arr == '3 1') {
        arr5_5_s[10] = 1;
      } else if (arr == '3 2') {
        arr5_5_s[11] = 1;
      } else if (arr == '3 3') {
        arr5_5_s[12] = 1;
      } else if (arr == '3 4') {
        arr5_5_s[13] = 1;
      } else if (arr == '3 5') {
        arr5_5_s[14] = 1;
      } else if (arr == '4 1') {
        arr5_5_s[15] = 1;
      } else if (arr == '4 2') {
        arr5_5_s[16] = 1;
      } else if (arr == '4 3') {
        arr5_5_s[17] = 1;
      } else if (arr == '4 4') {
        arr5_5_s[18] = 1;
      } else if (arr == '4 5') {
        arr5_5_s[19] = 1;
      } else if (arr == '5 1') {
        arr5_5_s[20] = 1;
      } else if (arr == '5 2') {
        arr5_5_s[21] = 1;
      } else if (arr == '5 3') {
        arr5_5_s[22] = 1;
      } else if (arr == '5 4') {
        arr5_5_s[23] = 1;
      } else if (arr == '5 5') {
        arr5_5_s[24] = 1;
      }
      for (var i = 0; i < 5; i++) {
        if (arr5_5_s[i] == 1) {
          if (arr5_5[i].toString().length == 2) {
            stdout.write('${arr5_5[i]} ');
          } else {
            stdout.write(' ${arr5_5[i]} ');
          }
          checkWin(arr5_5[i]);
        } else {
          stdout.write(' 0 ');
        }
      }
      print('');
      for (var i = 5; i < 10; i++) {
        if (arr5_5_s[i] == 1) {
          if (arr5_5[i].toString().length == 2) {
            stdout.write('${arr5_5[i]} ');
          } else {
            stdout.write(' ${arr5_5[i]} ');
          }
          checkWin(arr5_5[i]);
        } else {
          stdout.write(' 0 ');
        }
      }
      print('');
      for (var i = 10; i < 15; i++) {
        if (arr5_5_s[i] == 1) {
          if (arr5_5[i].toString().length == 2) {
            stdout.write('${arr5_5[i]} ');
          } else {
            stdout.write(' ${arr5_5[i]} ');
          }
          checkWin(arr5_5[i]);
        } else {
          stdout.write(' 0 ');
        }
      }
      print('');
      for (var i = 15; i < 20; i++) {
        if (arr5_5_s[i] == 1) {
          if (arr5_5[i].toString().length == 2) {
            stdout.write('${arr5_5[i]} ');
          } else {
            stdout.write(' ${arr5_5[i]} ');
          }
          checkWin(arr5_5[i]);
        } else {
          stdout.write(' 0 ');
        }
      }
      print('');
      for (var i = 20; i < 25; i++) {
        if (arr5_5_s[i] == 1) {
          if (arr5_5[i].toString().length == 2) {
            stdout.write('${arr5_5[i]} ');
          } else {
            stdout.write(' ${arr5_5[i]} ');
          }
          checkWin(arr5_5[i]);
        } else {
          stdout.write(' 0 ');
        }
      }
      print('');
    }
  }
  gamerun () {
    print('กรุณาเลือกขนาดของบอร์ดเกมส์');
    print('1) ขนาด 3 * 3');
    print('2) ขนาด 5 * 5');
    int? n1 = int.parse(stdin.readLineSync()!);
    if (n1 == 1) {
      s = 1;
      setarr3_3();
    } else if (n1 == 2) {
      s = 2;
      setarr5_5();
    }
    getTable();
    RandomOrder(mode);
    do {
      print('กรุณาเลือกตำแหน่งที่ต้องการ');
      print('Row Column : ');
      var n2 = stdin.readLineSync();
      print('P$p');
      print('trun : $t');
      showTable(n2);
      if (t == 4) {
        List<String> item = ['ผู้เล่นทั้งสอง ดื่ม ทั้งหมด 5 ฝา', 'ผู้เล่น 1 ดื่ม 1ฝา', 'ผู้เล่น 2 ดื่ม 1 ฝา', 'ผู้เล่น 1 วิ่งรอบบ้าน'];
        final _random = new Random();
        var element = item[_random.nextInt(item.length)];
        print(element);

      }
      else if(t == 10){
        List<String> item = ['ผู้เล่นทั้งสอง ดื่ม ทั้งหมด 2 ฝา', 'ผู้เล่น 1 หมุนตั๊กแตน 10 ครั้ง', 'ผู้เล่น 2 วิ่งรอบ ผู้เล่น 1', 'ผู้เล่น 1 วิ่งรอบบ้าน'];
        final _random = new Random();
        var element = item[_random.nextInt(item.length)];
        print(element);

      }
      showTurn();
      switchPlayer();
    } while (end < getCurrentPlayer());
    switchPlayer();
    showwin();
    isFinish(n1);
  }
  
}
void main(List<String> arguments) {
  Game run = new Game();
  run.gamerun();
}